# On fresh install

## READ ENTIRE DOCUMENTATION BEFORE DOING ANYTHING

```
cd www/Frontend

npm i

cd ..
```

Then run, this will take some time wait until it's done installing

```
docker build -t vueapp:0.0.1 ./Frontend
```
After running above command run one of the following commands, this will take at least 5 minutes, be patient and have a cup of coffee while waiting, strapi isn't the fastest

```
docker-compose up
```
or
```
docker-compose up -d
```

## To access the frontend

Go to localhost:8080

## To access Strapi 

go to localhost:1337
